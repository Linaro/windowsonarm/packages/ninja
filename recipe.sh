#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    git clone https://github.com/ninja-build/ninja
    pushd ninja
    git reset --hard $version
    git log -n1
    popd
}

build()
{
    pushd ninja
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release -GNinja ../
    ninja
    popd
}

package()
{
    mv ninja/build/ninja.exe $out_dir
}

checkout
build
package
